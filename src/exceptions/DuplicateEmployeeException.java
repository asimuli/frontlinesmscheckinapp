/**
 * 
 */
package exceptions;

/**
 * @author Alice China
 *
 */
public class DuplicateEmployeeException extends CheckInException
{

	public void fix()
	{
		System.out.println("You already entered that employee\n");
		//Self healing code goes here
	}
}
