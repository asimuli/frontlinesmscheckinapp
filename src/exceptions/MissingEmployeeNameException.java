/**
 * 
 */
package exceptions;

/**
 * @author Alice China
 *
 */
public class MissingEmployeeNameException extends CheckInException
{
	public void fix()
	{
		System.out.println("You have to enter at least one employee.\n");
		//Self healing code goes here
		//Exiting the program to facilitate the demonstration
		System.exit(0);
	}
}
