/**
 * 
 */
package exceptions;

/**
 * @author Alice China
 *
 */
public abstract class CheckInException extends Exception
{
	public abstract void fix();
}
