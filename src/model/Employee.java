/**
 * 
 */
package model;

import java.util.Date;

/**
 * @author Alice China
 *
 */
public class Employee
{
	private int employeeNumber;
	private boolean isManager;
	
	//Default constructor
	public Employee()
	{
		employeeNumber = 0;
		isManager = false;
	}
	
	//Constructor
	public Employee(int empNumber, boolean manager)
	{
		employeeNumber = empNumber;
		isManager = manager;
	}
	
	//Setters
	public void setEmployeeNumber(int empNumber) 
	{
		employeeNumber = empNumber;
	}
	
	public void setManager(boolean manager) 
	{
		isManager = manager;
	}

	//Getters
	public int getEmployeeNumber() 
	{
		return employeeNumber;
	}
	
	public boolean getManager()
	{
		return isManager;
	}
	
	@Override
	public String toString() {
		return "Employee [employeeNumber=" + employeeNumber + ", isManager=" + isManager + "]";
	}

}
