/**
 * 
 */
package model;

/**
 * @author Alice China
 *
 */
public class TeamMember extends Employee
{
	private String name;
	private boolean lateState;
	private boolean benefitState;
	
	//Default constructor
	public TeamMember()
	{
		name = "";
		lateState = false;
		benefitState = false;
	}
	
	//Constructor
	public TeamMember(String empName, boolean lState, boolean bState)
	{
		name = empName;
		lateState = lState;
		benefitState = bState;
	}
	
	public TeamMember(String empName)
	{
		name = empName;
		lateState = false;
		benefitState = false;
	}
	
	//Getters
	public String getName()
	{
		return name;
	}
	
	public boolean isLateState()
	{
		return lateState;
	}
	
	public boolean isBenefitState()
	{
		return benefitState;
	}
	
	//Setters
	public void setName(String empName)
	{
		name = empName;
	}
	
	public void setLateState(boolean lState)
	{
		lateState = lState;
	}
	
	public void setBenefitState(boolean bState)
	{
		benefitState = bState;
	}

	@Override
	public String toString() {
		return "TeamMember [name=" + name + ", lateState=" + lateState + ", benefitState=" + benefitState + "]";
	}
}
