/**
 * 
 */
package factory;

import exceptions.*;

/**
 * @author Alice China
 *
 */
public class CheckinExceptionsFactory
{
	public CheckInException getException(String exceptionType)
    {
        CheckInException exception = null;
        
        if (exceptionType.equalsIgnoreCase("DUPLICATE_EMPLOYEE_EXCEPTION"))
        {
            exception = new DuplicateEmployeeException();

        } 
        else if (exceptionType.equalsIgnoreCase("MISSING_EMPLOYEE_EXCEPTION"))
        {
            exception = new MissingEmployeeNameException();

        }

        return exception;
    }
}
