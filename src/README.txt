The following is a documentation for the FrontlineSMS CheckIn App

Description
------------

At Frontline�s Nairobi office, we hold daily standup meetings at 9:55. In order to prepare for and attend
these meetings, we expect dev team members to be in by 9:50am. In order to enforce this in a friendly/playful
way, we have a �coffee checkin�: whenever a team member is late, they have to buy coffee for one of their 
colleagues who was on time. Whenever someone gets a coffee, the next person down in the list becomes the 
new �next beneficiary�. If the �next beneficiary� is late themselves, they are skipped and must buy the next 
person a coffee.

Working of the program
----------------------
This program is a simple command line Java program that automates this checkin process.
The program:
 1. Prompts the user to provide the list of employees. The first one can be assumed to be �next� for Monday.
 2. Prompts the user to provide the list of late employees for Monday.
 3. Outputs the updated list, showing who should buy coffees, and who is next in line for Tuesday.
 4. Prompts the user to repeat step 2 above for the rest of the week, Tuesday through Friday.
 5. At the end of the week, prints out the final state and exits
 
 Assumptions
 ------------
 The following was assumed to be true for this case scenario:
 1. All employees are added to the program at the beginning, before the week begins.
 2. Employees keyed in as late were initially added as employees at the beginning of the week.
 3. A new week at Frontline means a clean slate for lateness and beneficiaries.
 
Implementation
--------------
We use 3 data structures to track information in the program:
 1. ArrayList - Stores the list of all employees. It is efficient because we do not know the what size of the list
    of employees will be at the beginning of each week. This allows for flexibility. It has useful predefined
    functions that make manipulation easier.
 
 2. Stack - Stores the list of late employees. It is useful because we want a LIFO list since, in case more than one
    employee were late on a particular day, we want the employee who was most late should buy the coffee. It also has 
    useful predefined functions that make manipulation easier.
    
 3. Queue - Stores the list of beneficiaries. It is convenient to use a queue for this case because we want a FIFO list
    so that the each employee gets a chance to be a beneficiary in the order in which they were keyed in at the beginning
    of the week. It also has useful predefined functions that make manipulation easier.
    
To keep track of the days of the week, we use an enumeration. The first advantage of this is that enums are iterable,
so we can a provide a simple way to iterate through the week by using them. Secondly, we want to ensure we have a rigid set
of work days since they are fixed. Also, suppose we want to work on Saturdays too, we can just add the extra day to our enum
and we won't need to change anything in the rest of the code.

Inheritance has been used to demonstrate the relationship between an Employee and a TeamMember. An Employee is a TeamMember.
This relationship can provide us an opportunity to enforce encapsulation and also make use of polymorphism (which is very
powerful). This can be seen in the exception hierarchy.

We have also implemented one design pattern - Factory pattern, to handle exceptions. This helps us not to get into the details
of figuring out object to instantiate for each exception.



Future work
-----------

Some ideas on how to further improve the program:

1. Allow file input for the information required. We can read the names of the employees from a text file.
2. Generate status reports for every week and write to a pdf file for record keeping.
3. Implement self healing, such that exceptions captured are corrected to avoid crushing.
4. Enforce loose coupling for classes used.