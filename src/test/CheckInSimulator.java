/**
 * 
 */
package test;
import model.*;
import exceptions.*;
import factory.*;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * @author Alice China
 *
 */
public class CheckInSimulator {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		/*Declaring and initializing the variables we need
		------------------------------------------------*/
		
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		String empName = "";
		String employeeName = "";
		boolean benefited = false;
		String nextBeneficiary = "";
		String lateMembers = "";
		TeamMember teamMember;
		
		ArrayList<TeamMember> employees = new ArrayList<TeamMember>();
		Queue<String> beneficiaryList = new LinkedList<>();
		Stack<String> lateList = new Stack<>();
		
		WeekDay weekdays[] = WeekDay.values();
		
		//Prompt the user in order to populate our data structures
		
		 System.out.println("Welcome to FrontlineSMS Coffee Check In App.\n\n Please "
		 		+ "enter the list of all employees, one at a time.\n (Press \'Enter\' when done)\n");
	        
	     empName = keyboard.readLine();
	     
	     /*The following is a way to keep prompting the user until they enter a valid name (not blank).

     	 while (empName.equals(""))
	     {
	    	 System.out.println("Employee List cannot be empty. Please enter a name.\n");
	    	 empName = keyboard.readLine();
 		 }*/
	     
	     //But for demonstration purposes let's use a custom exception and exit the program when this happens.
	     
	     //Declare the custom exception and factory object
	     
	     CheckinExceptionsFactory checkinExceptionsFactory = new CheckinExceptionsFactory();
	     
	     try
         {
		     if (empName.equals(""))
		     {
		    	 CheckInException exception = checkinExceptionsFactory.getException("missing_employee_exception");
		    	 throw exception;
		     }
         }
	     catch(CheckInException e)
	     {
	    	 e.fix();
	     }
	     
	     while(! empName.equals(""))
	     {
	    	 teamMember = new TeamMember(empName);
	    	 
	    	 //Add employees to the employees array list
	    	 employees.add(teamMember);
	    	 
	    	 //Add employees to the beneficiaries queue
	    	 beneficiaryList.add(empName);
	    	 
	         System.out.println("\nEnter another employee: ");
	         empName = keyboard.readLine();
	     }
	     
	     System.out.println("Thanks! All team members added.\n***************\n");
	     
	     //Print out the initial state of the list before the start of the week
	     
	     for (TeamMember t: employees)
	     {
	    	 System.out.print(t.getName());
	    	 
	    	 //Mark the first employees entered as 'next beneficiary'
	    	 
	    	 if (beneficiaryList.peek().equalsIgnoreCase(t.getName()))
	    	 {
	    		 System.out.print(" <- next");
	    	 }
	    	 System.out.println();
	     }
	     
	     //Loop through each the day of the week, prompting the user for late employees and
	     //updating the list accordingly
	     
	     for(WeekDay weekday: weekdays)
		 {
	    	 System.out.println("\n\nGood Morning. Happy "+ weekday + "!!! \n Who's late today?\n");
	    	 lateMembers = keyboard.readLine();
		    	 
	    	 //Add late employees to the late employees stack
	    	 
		     while(! lateMembers.equals(""))
		     {
		    	 lateList.add(lateMembers);
		    	 System.out.println("Who else?\n");
		    	 lateMembers = keyboard.readLine();
		     }
		     
		     if(lateList.isEmpty())
		     {
		    	 System.out.println("***Yay!! Noone was late today!***\n");
		     }
		     
		     //Loop through the employees and printing their names
		     
		     for (TeamMember emp: employees)
		     {
		    	 employeeName = emp.getName();
		    	 System.out.print(employeeName);
		    	 
		    	 //Checks to identify names that need to be labeled appropriately (next, gets coffee or late)
		    	 if(!(beneficiaryList.isEmpty()) && !(lateList.isEmpty()) && beneficiaryList.peek().equalsIgnoreCase(lateList.peek()))
		    	 {
		    		 beneficiaryList.remove();
		    	 }
		    	 
		    	 if(!(lateList.isEmpty()) && lateList.peek().equalsIgnoreCase(employeeName))
		    	 {
		    		 System.out.print(" <- late");
		    	 }
		    	 else if(!(beneficiaryList.isEmpty()) && beneficiaryList.peek().equalsIgnoreCase(employeeName))
		    	 {
		    		 if(!benefited && !(lateList.isEmpty()))
		    		 {
			    		 System.out.print(" <- gets coffee from "+ lateList.peek());
			    		 beneficiaryList.remove();
			    		 nextBeneficiary = beneficiaryList.peek();
			    		 benefited = true;
		    		 }
		    		 else if(employeeName.equalsIgnoreCase(nextBeneficiary))
			    	 {
			    		 System.out.print(" <- next");
			    	 }
		    	 }
		    	 
		    	 System.out.println();
		     }
		     benefited = false;
		 }
	     System.out.println("\n*****Have a GREAT weekend folks!*****\n");
	}
}
